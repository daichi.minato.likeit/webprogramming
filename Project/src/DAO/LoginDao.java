package DAO;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import Model.User;

public class LoginDao {
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	   * 全てのユーザ情報を取得する
	   * @return
	   */
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE NOT (login_id = 'admin')";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public int findByRegister(String loginId, String name, String birthDate,
			String password) {
		Connection conn = null;
		int rs = 0;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "INSERT INTO user (login_Id, name,birth_date,password,create_date,update_date)VALUES(?,?,?,?,now(),now())";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, password);

			rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return (Integer) null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return (Integer) null;
				}
			}
		}
		return rs;
	}

	public User findByDetails(String Id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, Id);
			ResultSet rs1 = pStmt.executeQuery();
			if (!rs1.next()) {
				return null;
			}
			int IdData = rs1.getInt("Id");
			String loginidData = rs1.getString("login_id");
			String nameData = rs1.getString("name");
			Date birthDateData = rs1.getDate("birth_date");
			String passwordData = rs1.getString("password");
			String createDateData = rs1.getString("create_date");
			String updateDateData = rs1.getString("update_date");
			return new User(IdData, loginidData, nameData, birthDateData, passwordData, createDateData, updateDateData);

		} catch (SQLException e) {
			e.printStackTrace();
			return (User) null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public int findByUpdate(String loginId, String password, String password2, String name, String birthDate,
			String pas) {
		//nullジャッチ
		//pass,pass2比較
		//
		if (name == null || birthDate == null) {
			return 0;
		}
		if (!password.equals(password2)) {
			return 0;
		}
		Connection conn = null;
		int rs = 0;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "UPDATE user SET password = ?,name = ?,birth_date = ?,update_date = now() WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, pas);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, loginId);
			rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return (Integer) null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return (Integer) null;
				}
			}
		}
		return rs;
	}

	public int findByElimination(String Id) {
		//nullジャッチ
		//pass,pass2比較

		Connection conn = null;
		int rs = 0;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "DELETE FROM user WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, Id);

			rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return (Integer) null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return (Integer) null;
				}
			}
		}
		return rs;
	}

	public List<User> findByList2(String loginId, String name, String brithDate, String brithDate2) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE NOT (login_id = 'admin')";

			if (!loginId.equals("")) {
				sql += "and login_id = '" + loginId + "'";
			}
			if (!name.equals("")) {
				sql += " and name LIKE '%" + name + "%'";
			}
			if (!brithDate.equals("")) {
				sql += "and birth_date>='" + brithDate + "'";
			}
			if (!brithDate2.equals("")) {
				sql += "and birth_date<='" + brithDate2 + "'";
			}

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);

			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId1 = rs.getString("login_id");
				String name1 = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId1, name1, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public String findByPassword(String password) {
		String source = password;
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);
		return result;

	}


}
