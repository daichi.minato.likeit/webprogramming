package Servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.LoginDao;
import Model.User;

/**
 * Servlet implementation class Elimination
 */
@WebServlet("/Elimination")
public class Elimination extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Elimination() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("List");
		if(user==null) {
			response.sendRedirect("loginServlet");
		return;
		}
		String Id = request.getParameter("id");
		//System.out.println(Id);

		LoginDao loginDao = new LoginDao();
		User user1 = loginDao.findByDetails(Id);
		request.setAttribute("user", user1);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Elimination.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		LoginDao loginDao = new LoginDao();
		int result = loginDao.findByElimination(id);
		if (result == 0) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Elimination.jsp");
			dispatcher.forward(request, response);
			return;
		}
		response.sendRedirect("List");

	}

}
