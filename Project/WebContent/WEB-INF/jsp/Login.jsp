<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>
<body>

	<h1>ログイン画面</h1>


	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	<form class="form-signin" action="loginServlet" method="post">
		<div class="container-fluid">
			<label for="exampleFormControlSelect1">ログインID</label> <input
				type="text" name="loginId" size="30">
		</div>
		<div class="form-group">
			<label for="exampleFormControlSelect1">パスワード</label> <input
				type="password" name="password" size="30">
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-primary">ログイン</button>
		</div>
	</form>
</body>
</html>
