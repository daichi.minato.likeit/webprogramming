<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>
<body>

	<div class="form-group">
		<label for="exampleFormControlSelect1">${List.name}さん</label>
	</div>
	<button type="button" class="btn btn-success">
		<a href="logout">ログアウト</a>
	</button>
	<button type="button" class="btn btn-info">
		<a href="RegisterServlet">新規登録</a>
	</button>
	<h1>ユーザー一覧</h1>
	<form class="form-signin" action="List" method="post">

		<div class="container-fluid">
			<label for="exampleFormControlSelect1">ログインID</label> <input
				type="text" name="loginId" size="30">
		</div>
		<div class="form-group">
			<label for="exampleFormControlSelect1">ユーザー名</label> <input
				type="text" name="name" size="30">
		</div>
		<div class="form-group">
			<label for="exampleFormControlSelect1">生年月日</label> <input
				type="date" name="birthDate"> <label
				for="exampleFormControlSelect1">~</label>
		</div>
		<input type="date" name="birthDate2">
		<div class="container-fluid">
			<button type="submit" class="btn btn-primary" href="List">検索</button>
		</div>

	</form>
	<div class="form-group">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>ログインID</th>
					<th>ユーザ名</th>
					<th>生年月日</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="user" items="${userList}">
					<tr>
						<td>${user.loginId}</td>
						<td>${user.name}</td>
						<td>${user.birthDate}</td>
						<!-- TODO 未実装；ログインボタンの表示制御を行う -->
						<td><a class="btn btn-primary" href="Details?id=${user.id}">詳細</a>
							<c:if test="${List.name=='管理者' }">
								<a class="btn btn-success" href="Update?id=${user.id}">更新</a>
							</c:if>
							<c:if test="${List.name=='管理者' }">
								<a class="btn btn-danger" href="Elimination?id=${user.id}">削除</a>
								</c:if>
								</td>

					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

</body>
</html>

