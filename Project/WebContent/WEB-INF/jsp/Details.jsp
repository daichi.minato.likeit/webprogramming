<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
<div class="form-group">
			<label for="exampleFormControlSelect1">${List.name}さん</label>
		</div>
	<button type="button" class="btn btn-success">
		<a href="logout">ログアウト</a>
	</button>
	<h1>ユーザー情報詳細参照</h1>

	<div class="container-fluid">
		<label for="exampleFormControlSelect1">ログインID</label>
		${user.loginId}
	</div>
	<div class="container-fluid">
		<label for="exampleFormControlSelect1">ユーザー名</label>
		${user.name}
	</div>
	<div class="container-fluid">
		<label for="exampleFormControlSelect1">生年月日</label>
		${user.birthDate}
	</div>
	<div class="container-fluid">
		<label for="exampleFormControlSelect1">登録日時</label>
		${user.createDate}
	</div>
	<div class="container-fluid">
		<label for="exampleFormControlSelect1">更新日時</label>
		${user.updateDate}
	</div>
	<div class="container-fluid">
		<button type="button" class="btn btn-warning">
			<a href="List">戻る</a>
		</button>
	</div>
</body>
</html>
