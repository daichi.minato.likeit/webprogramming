<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>title</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>
<body>
<div class="form-group">
			<label for="exampleFormControlSelect1">${List.name}さん</label>
		</div>
	<c:if>
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>

	<button type="button" class="btn btn-success">
		<a href="logout">ログアウト</a>
	</button>
	<h1>ユーザー新規登録</h1>
	<form method="post" action="RegisterServlet" class="form-horizontal">
		<div class="container-fluid">
			<label for="exampleFormControlSelect1">ログインID</label> <input
				type="text" name="login_id" size="30">


		</div>
		<div class="form-group">
			<label for="exampleFormControlSelect1">パスワード</label> <input
				type="password" name="password" size="30">


		</div>
		<div class="form-group">
			<label for="exampleFormControlSelect1">パスワード確認</label> <input
				type="password" name="password2" size="30">

		</div>
		<div class="form-group">
			<label for="exampleFormControlSelect1">ユーザー名</label> <input
				type="text" name="name" size="30">

		</div>
		<div class="form-group">
			<label for="exampleFormControlSelect1">生年月日</label> <input
				type="date" name="birthDate" value="1990-01-01">

		</div>
		<div class="container-fluid">
			<button type="submit" class="btn btn-primary">登録</button>

		</div>
	</form>
	<div class="container-fluid">
		<button type="button" class="btn btn-warning">
			<a href="List">戻る</a>
		</button>
	</div>
</body>
</html>

